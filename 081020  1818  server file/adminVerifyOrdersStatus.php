<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pending Orders  | PPay" />
<title>Pending Orders | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance min-height2">
	<h1 class="green-text h1-title opacity-hover"  onclick="goBack()"><img src="img/back-3.png" class="back-img"><?php echo _ORDERS_NUMBER ?>: #<?php echo $_POST['order_id'];?></h1>
    <div class="green-border"></div>


    <div class="clear"></div>

    <form method="POST" id="paymentVerifiedForm" onsubmit="doPreview(this.submited); return false;">
        <div class="width100 scroll-div2 border-separation">
            <?php
            if(isset($_POST['order_id']))
            {
            $orderId = $_POST['order_id'];
            $conn = connDB();
            $orders = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");
            // $orders = getProductOrders($conn);
            $order = getOrders($conn);
            ?>
                <div class="width100 overflow">
                    <p class="green-text top-text"><?php echo _ORDERS_DETAILS ?></p>
                    <div class="table-scroll-div">
                        <table class="order-table ow-width100">
                            <thead>	
                                <tr>
                                    <th><b><?php echo _ORDERS_NO ?></b></th>
                                    <th><b><?php echo _INDEX_PRODUCT ?></b></th>
                                    <th><b><?php echo _ADMIN_QUANTITY ?></b></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                for($cnt = 0;$cnt < count($orders) ;$cnt++)
                                {
                                    if($orders[$cnt]->getOrderId() == $orderId)
                                    {
                                    ?>
                                        <tr>
                                            <td><?php echo $cnt+1;?></td>
                                            <td><?php echo $orders[$cnt]->getProductName();?></td>
                                            <td><?php echo $orders[$cnt]->getQuantity();?></td>
                                        </tr>
                                    <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="clear"></div>
            <?php
            }
            ?>

            <?php
            if(isset($_POST['order_id']))
            {
            $conn = connDB();
            $orders = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"s");
            ?>
                <div class="width100 overflow margin-top30">
                    <p class="green-text top-text"><?php echo _ADMIN_DELIVERY_ADDRESS ?></p>
                    <p class="bottom-text"><?php echo $orders[0]->getName();?></p>  
                    <p class="bottom-text"><?php echo $orders[0]->getContactNo();?></p>
                    <p class="bottom-text"><?php echo $orders[0]->getAddressLine1();?></p>
                </div>
            <?php
            }
            ?>

        </div>
		<div class="width100 overflow  margin-top30">
            <div class="dual-div">
                <p class="green-text top-text"><?php echo _ORDERS_SHIPPING_METHOD ?></p>
                <input class="input-name clean input-textarea admin-input date-input" type="text" placeholder="<?php echo _ORDERS_SHIPPING_METHOD ?>" id="shipping_method" name="shipping_method">
                <!-- <input class="input-name clean input-textarea admin-input date-input" type="text" placeholder="Shipping Method" id="shipping_method" name="shipping_method" required>             -->
            </div>
            <div class="dual-div second-dual-div">
                <p class="green-text top-text"><?php echo _ORDERS_TRACKING_NUMBER ?></p>
                <input class="input-name clean input-textarea admin-input date-input" type="text" placeholder="<?php echo _ORDERS_TRACKING_NUMBER ?>" id="tracking_number" name="tracking_number"> 
            </div>       
        </div>
        
        <div class="clear"></div>
        
		<div class="width100 overflow">
            <div class="dual-div">
            <p class="green-text top-text"><?php echo _ADMIN_ISSUE_DATE ?></p>
                <input class="input-name clean input-textarea admin-input date-input" type="date" placeholder="<?php echo _ADMIN_ISSUE_DATE ?>" id="delivered_on" name="delivered_on" required> 
            </div> 
		</div>
        <div class="clear"></div>

        <div class="dual-div">
            <input class="input-name clean input-textarea admin-input date-input" type="hidden" id="order_id" name="order_id" value="<?php echo $_POST['order_id'];?>"> 
        </div> 

        <input class="input-name clean input-textarea admin-input date-input" type="hidden" value="<?php echo $orders[0]->getSubtotal();?>" id="diamond" name="diamond" readonly> 
        <input class="input-name clean input-textarea admin-input date-input" type="hidden" value="<?php echo $orders[0]->getUid();?>" id="user_uid" name="user_uid" readonly> 
        
        <div class="width100 overflow text-center">   
            <input onclick="this.form.submited=this.value;"  type="submit" name="REJECT" value="REJECT" class="float-left red-btn white-text clean2 edit-1-btn margin-auto bottom-reject">
            <input onclick="this.form.submited=this.value;"  type="submit" name="ACCEPTED" value="ACCEPTED" class="float-left green-button white-text clean2 edit-1-btn second-button-margin" >
        </div>

    </form>

    <div class="clear"></div>

    

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function goBack() 
{
    window.history.back();
}
</script>


<script type="text/javascript">
    function doPreview(buttonType)
    {
        switch(buttonType){
            case 'ACCEPTED':
                form=document.getElementById('paymentVerifiedForm');
                // form.action='shippingRefund.php';
                form.action='utilities/updateOrdersAcceptedFunction.php';
                form.submit();
            break;
            case 'REJECT':
                form=document.getElementById('paymentVerifiedForm');
                form.action='utilities/updateOrdersRejectedFunction.php';
                form.submit();
            break;
        }

    }
</script>

</body>
</html>