<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Shipping.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['user_id'];

$conn = connDB();
$date = date("Y-m-d");
$time = date("h:i a");

$id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$orderUid = $id[0]->getId();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = connDB();
    
    $uid = ($_POST['uid']);

    $name = ($_POST["insert_name"]);
    $contactNo = ($_POST["insert_contactNo"]);
    $address_1 = ($_POST["insert_address_1"]);
    $subtotal = ($_POST["subtotal"]);

    if(isset($_POST['shippingSubmit']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
    
        //echo "save to database";
        if($name)
        {
            array_push($tableName,"name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
    
        if($contactNo)
        {
            array_push($tableName,"contactNo");
            array_push($tableValue,$contactNo);
            $stringType .=  "s";
        }
    
        if($address_1)
        {
            array_push($tableName,"address_line_1");
            array_push($tableValue,$address_1);
            $stringType .=  "s";
        }

        if($subtotal)
        {
            array_push($tableName,"subtotal");
            array_push($tableValue,$subtotal);
            $stringType .=  "d";
        }

        // echo $uid. "<br>";
        // echo $name. "<br>";
        // echo $contactNo. "<br>";
        // echo $address_1. "<br>";
        // echo $subtotal. "<br>";

    
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $updateOrderDetails = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($updateOrderDetails)
        {
            // echo "<script>alert('Successfully added shipping details!');</script>";
        }
        else
        {
            echo "<script>alert('Fail to add shipping details!');window.location='../checkout.php'</script>"; 
        }
    }
    else
    {
        echo "<script>alert('ERROR 2');window.location='../checkout.php'</script>"; 
    }

         
}
else 
{
     header('Location: ../index.php');
}

// $userRows = getUser($conn," WHERE user_id = ? ",array("user_id"),array($uid),"s");
// $userDetails = $userRows[0];

// $userOrder = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
// $orderDetails = $userOrder[0];

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}else
{}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Shipping | PPay" />
<title>Shipping | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay,e-commerce,iphone,phone,huawei">

 <link rel="stylesheet" type="text/css" href="css/glider.css">
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

    <div class="two-menu-space width100"></div>    
        <div class="width100 same-padding min-height4 shipping-big-div">
            <form method="POST" action="invoice.php" enctype="multipart/form-data">

                <input class="clean white-input two-box-input" type="hidden" id="order_id" name="order_id" value="<?php echo $id;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_username" name="insert_username" value="<?php echo $username;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_name" name="insert_name" value="<?php echo $name;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_contactNo" name="insert_contactNo" value="<?php echo $contactNo;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_address_1" name="insert_address_1" value="<?php echo $address_1;?>">

                <?php
                    if(isset($_POST['uid']))
                    {
                        $conn = connDB();
                        $orders = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['uid']),"s");
                        $userRows = getUser($conn," WHERE user_id = ? ",array("user_id"),array($orders[0]->getUid()),"s");
                        $totalDiamond = $orders[0]->getSubtotal();
                        $onhandDiamond = $userRows[0]->getDiamond();
                    ?>


                <?php
                }
                ?>

                <?php
                if($onhandDiamond >= $totalDiamond){
                    ?>
                    <div class="clear"></div>
                    <div class="dual-input">
                        <input type="hidden" id="uid" name="uid" value="<?php echo $orderUid ?>">
                    </div>

                    <div class="dual-input">
                        <input type="hidden" id="userId" name="userId" value="<?php echo $_SESSION['user_id']?>">
                    </div>

                    <input type="hidden" value="PENDING" class="clean edit-profile-input payment-input" id="payment_status" name="payment_status">
                    <input type="hidden" value="PENDING" class="clean edit-profile-input payment-input" id="shipping_status" name="shipping_status">
                    <input type="hidden" value="Points" class="clean edit-profile-input payment-input" id="payment_method" name="payment_method">
                    <input type="hidden" class="clean edit-profile-input payment-input" id="total_diamond" name="total_diamond" value="<?php echo $totalDiamond;?>">
                    <input type="hidden" class="clean edit-profile-input payment-input" id="onhand_diamond" name="onhand_diamond" value="<?php echo $onhandDiamond;?>">
                    <div class="clear"></div>
                    
                    <div class="right-status-div">
                    <?php echo $productListHtml; ?>
                    </div>

                    <div class="sticky-bottom-price same-padding3">
                        <div class="width100 text-center">                                                                         
                            <button class="green-button checkout-btn clean" id="next" name="next" type="submit"><?php echo _USER_COMPLETE_ORDER ?></button>
                            </br>
                            <a href="checkout.php" class="green-a"><?php echo _USER_BACK ?></a>
                        </div>
                    </div>
                <?php
                }else {
                    ?>
                    <div class="clear"></div>   
                    <div class="right-status-div">
                    <?php echo $productListHtml; ?>
                    </div>

                    <div class="sticky-bottom-price width100 overflow text-center margin-bottom-20px">
                    <div class="dual-input">
                        <p class="input-top-p"><?php echo _USER_TOTAL_DIAMONDS_DEDUCT ?></p>
                        <p class="no-input"><?php echo $totalDiamond;?> <?php echo _PRODUCT_DIAMOND ?></p>
                        <input class="clean white-input two-box-input" type="hidden" id="insert_total_diamond" name="insert_total_diamond" value="<?php echo $totalDiamond;?>">
                    </div>

                    <div class="dual-input second-dual-input">
                        <p class="input-top-p"><?php echo _USER_AVAILABLE_DIAMONDS ?></p>
                        <p class="no-input"><?php echo $onhandDiamond;?> <?php echo _PRODUCT_DIAMOND ?></p>
                        <input class="clean white-input two-box-input" type="hidden" id="insert_onhand_diamond" name="insert_onhand_diamond" value="<?php echo $onhandDiamond;?>">
                    </div>
                    <div class="clear"></div>                    
                        <p><?php echo _USER_INSUFFICIENT_DIAMONDS ?></p>
                        <input type="button" class="green-button checkout-btn clean" onclick="location.href='profile.php';" value="<?php echo _USER_CONVERT_POINTS_TO_DIAMONDS ?>" />
                        </br>
                        <a href="checkout.php" class="green-a"><?php echo _USER_BACK ?></a>
                    </div>
                   

                <?php
                }
                ?>

            </form>
        </div>
    </div>

    <?php include 'js.php'; ?>

</body>

    <script>

    function la(src){
        window.location=src;
    }
    </script>
 

<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.green-footer{
		display:none;}
</style>
<div class="clear"></div>
<div class="width100 same-padding green-footer cart-footer">
	<p class="footer-p white-text">© 2020 <?php echo _USER_PPAY_COPYRIGHT ?></p>
</div>
<?php include 'js.php'; ?>

</html>