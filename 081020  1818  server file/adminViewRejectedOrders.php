<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

// $userDetails = getUser($conn," WHERE user_type = 1");
// $products = getOrders($conn, "WHERE payment_status = 'Pending' ");
// $orders = getOrders($conn, "WHERE shipping_status = 'Rejected' ");
$orders = getOrders($conn, "WHERE payment_status = 'REJECTED' AND shipping_status = 'REJECTED' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ORDERS_REJECTED ?>  | PPay" />
<title><?php echo _ORDERS_REJECTED ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance">

	<div class="width100">
  
            <h1 class="green-text h1-title"><?php echo _ORDERS_REJECTED ?></h1>
            <div class="green-border"></div>

    </div>

    <div class="clear"></div>

	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100">
        	<thead>
            	<tr>
                	<th><?php echo _ORDERS_NO ?></th>
                    <th><?php echo _ORDERS_ORDERID ?></th>
                    <th><?php echo _ORDERS_USERNAME ?></th>
                    <!-- <th><?php //echo _ORDERS_CONTACT ?></th> -->
                    <th><?php echo _ORDERS_DATE ?></th>
                    <th><?php echo _ORDERS_ACTION ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($orders)
                    {
                        for($cnt = 0;$cnt < count($orders) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $orders[$cnt]->getId();?></td>
                                <td><?php echo $orders[$cnt]->getName();?></td>
                                <!-- <td><?php //echo $orders[$cnt]->getContactNo();?></td> -->
                                <td><?php echo $orders[$cnt]->getDateCreated();?></td>
                                <td>
                                    <form action="adminViewInvoice.php" method="POST" class="hover1">
                                        <button class="clean blue-button2" type="submit" name="orderId" value="<?php echo $orders[$cnt]->getId();?>">
                                            <?php echo _ORDERS_VIEW ?>
                                        </button>
                                    </form> 
                                </td>
                            </tr>
                        <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>

    <div class="clear"></div>

    <div class="width100 bottom-spacing"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Orders Status as Rejected"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update orders status"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !"; 
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Fail to refund Diamond to user !!"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "ERROR on Users table !!"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>