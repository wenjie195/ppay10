<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$userId = $_SESSION['user_id'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_id =?",array("user_id"),array($userId),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Profile | PPay" />
<title>Profile | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

	<div class="width100 same-padding overflow min-height2 menu-distance">

                <p class="profile-greeting text-center"><?php echo PROFILE_HI ?>, <?php echo $userData->getUsername();?> <a href="editProfile.php" class="green-a edit-a hover1"><img src="img/edit.png" class="edit-profile-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-profile-png hover1b" alt="Edit" title="Edit"></a></p>
                


        <div class="clear"></div>
        <div class="width100 margin-top30">
            <div class="white-dropshadow-box two-shadow-box">
            		<img src="img/points.png" class="point-png" alt="Current Points" title="Current Points">
                    <!-- <p class="div-p">Current Points</p> -->
                    <p class="div-p"><?php echo PROFILE_CURRENT_PTS ?></p>
                    <p class="div-p div-amount"><?php echo $userData->getPoint();?><?php echo PROFILE_PTS ?></p>
            
            </div>
            <div class="white-dropshadow-box two-shadow-box second-two-shadow-box">
            		<img src="img/diamond.png" class="point-png" alt="Diamonds" title="Diamonds">
                    <!-- <p class="div-p">Diamonds</p> -->
                    <p class="div-p"><?php echo PROFILE_DIAMOND ?></p>
                    <p class="div-p div-amount"><?php echo $userData->getDiamond();?></p>
            
            </div>    
        </div>    
		<div class="clear"></div>
        <div class="width100 text-center overflow margin-top30">
            <!-- <a href="transferPoints.php" class="blue-gradient-button">Points to Diamonds</a> -->
            <a href="transferPoints.php" class="blue-gradient-button"><?php echo PROFILE_PTS_DIAMOND ?></a>
		</div>

        <div class="clear"></div>
        
        <!-- <a href="logout.php">
            <div class="profile-line-divider opacity-hover">
                <div class="left-icon-div"><img src="img/logout.png" class="left-icon-png" alt="Logout" title="Logout"></div>
                <div class="right-profile-content">Logout</div>
            </div>
        </a>                         -->
    </div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Profile Successfully !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Update Password Successfully !"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Update Shipping Address Successfully !"; 
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Profile Picture Updated !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Convert Points to Diamonds Successfully !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to convert points to diamond !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Fail to update users points and diamonds details !!"; 
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "ERROR !!"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "INSUFFICIENT POINTS !!"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>