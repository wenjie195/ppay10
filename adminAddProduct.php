<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$userId = $_SESSION['user_id'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_id =?",array("user_id"),array($userId),"s");
$userData = $userDetails[0];

$userAmount = getUser($conn," WHERE user_type = 1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_ADD_PRODUCT ?> | PPay" />
<title><?php echo _ADMIN_ADD_PRODUCT ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
  <div class="width100">
    <h1 class="green-text h1-title"><?php echo _ADMIN_ADD_PRODUCT ?></h1>
    <div class="green-border"></div>
  </div>

  <div class="clear"></div>

  <form action="utilities/registerProductFunction.php" method="POST" enctype="multipart/form-data">
    <div class="border-separation">
      <div class="dual-input">
        <!-- <p class="input-top-p admin-top-p">Category* <a href="category.php" class="green-a">(Add New Category Here)</a></p>
        <select class="input-name clean admin-input" placeholder="Category" name="register_category" id="register_category" required>
          <option>Puppy</option>
          <option>Kitten</option>
          <option>Reptile</option>
          <option>Other</option>
        </select> -->
        <p class="input-top-p admin-top-p"><?php echo _ADMIN_CATEGORY ?> <a href="adminAddCategory.php" class="green-a">(<?php echo _ADMIN_ADD_NEW_CATEGORY ?>)*</a></p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="<?php echo _ADMIN_CATEGORY ?>" name="register_category" id="register_category" required>
      </div>
      <div class="dual-input second-dual-input">
        <!-- <p class="input-top-p admin-top-p">Brand* <a href="brand.php" class="green-a">(Add New Brand Here)</a></p>
        <select class="input-name clean admin-input" placeholder="Brand" name="register_brand" id="register_brand" required>
          <option>Puppy</option>
          <option>Kitten</option>
          <option>Reptile</option>
          <option>Other</option>
        </select> -->
        <p class="input-top-p admin-top-p"><?php echo _ADMIN_BRAND ?></p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="<?php echo _ADMIN_BRAND ?>" name="register_brand" id="register_brand" required>
      </div>

      <div class="clear"></div>

      <div class="dual-input">
        <p class="input-top-p admin-top-p"><?php echo _ADMIN_PRODUCT_NAME2 ?>*</p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="<?php echo _ADMIN_PRODUCT_NAME2 ?>" name="register_name" id="register_name" required>
      </div>

      <div class="dual-input second-dual-input">
        <p class="input-top-p admin-top-p"><?php echo _ADMIN_PRICE ?></p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="<?php echo _ADMIN_PRICE ?>" name="register_price" id="register_price" required>
      </div>

      <div class="clear"></div>

      <div class="dual-input">
        <p class="input-top-p admin-top-p"><?php echo _PRODUCT_DIAMOND ?></p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="<?php echo _PRODUCT_DIAMOND ?>" name="register_diamond" id="register_diamond" required>
      </div>


      <div class="clear"></div>

      <div class="width100 overflow">
        <p class="input-top-p admin-top-p"><?php echo _ADMIN_PRODUCT_DESC ?>* (<?php echo _ADMIN_AVOID_KEYIN ?> "'')</p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="<?php echo _ADMIN_PRODUCT_DESC ?>" name="register_description" id="register_description" required>
      </div>

      <div class="clear"></div>

      <div class="width100 overflow margin-bottom10">
        <p class="input-top-p admin-top-p"><?php echo _ADMIN_UPLOAD_PRODUCT_PHOTO ?></p>
        <p><input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" class="margin-bottom10 pointer" /></p>
      </div>

    </div>

    <div class="clear"></div>

    <div class="width100 overflow text-center">
      <button class="green-button white-text clean2 edit-1-btn margin-auto"><?php echo _ADMIN_NEXT ?></button>
    </div>
  </form>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<!-- <div class="width100 same-padding green-footer">
	<p class="footer-p white-text">© 2020 Mypetslibrary, All Rights Reserved.</p>
</div> -->

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new product!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Registration of new product failed!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>