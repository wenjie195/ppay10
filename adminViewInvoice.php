<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ORDERS_DETAILS ?>  | PPay" />
<title><?php echo _ORDERS_DETAILS ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance min-height2">

    <h1 class="green-text h1-title opacity-hover" onclick="goBack()">
        <img src="img/back-3.png" class="back-img">
            <?php echo _ORDERS_NUMBER ?>: #<?php echo $_POST['orderId'];?>
    </h1>
	<div class="green-border"></div>
    <div class="clear"></div>

        <div class="width100 scroll-div2 border-separation">
            <?php
            if(isset($_POST['orderId']))
            {
            $orderId = $_POST['orderId'];
            $conn = connDB();
            $orders = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['orderId']),"i");
            // $orders = getProductOrders($conn);
            $order = getOrders($conn);
            ?>
                <div class="width100 overflow">
                    <p class="green-text top-text"><?php echo _ORDERS_DETAILS ?></p>
                    <div class="table-scroll-div">
                        <table class="order-table">
                            <thead>	
                                <tr>
                                    <th><b><?php echo _ORDERS_NO ?></b></th>
                                    <th><b><?php echo _INDEX_PRODUCT ?></b></th>
                                    <th><b><?php echo _ADMIN_QUANTITY ?></b></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                for($cnt = 0;$cnt < count($orders) ;$cnt++)
                                {
                                    if($orders[$cnt]->getOrderId() == $orderId)
                                    {
                                    ?>
                                        <tr>
                                            <td><?php echo $cnt+1;?></td>
                                            <td><?php echo $orders[$cnt]->getProductName();?></td>
                                            <td><?php echo $orders[$cnt]->getQuantity();?></td>
                                        </tr>
                                    <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php
            }
            ?>

            <?php
            if(isset($_POST['orderId']))
            {
            $conn = connDB();
            $orders = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['orderId']),"s");
            ?>
                <div class="width50-div overflow some-margin-bottom margin-top30">
                    <p class="green-text top-text"><?php echo _ORDERS_DELIVERY_DETAILS ?></p>
                    <table class="small-table">
                    	<tr>
                    		<td><?php echo _EDITPRO_NAME ?></td>
                            <td>:</td>
                        	<td><b><?php echo $orders[0]->getName();?></b></td>
                        </tr>
                    	<tr>
                    		<td><?php echo _ORDERS_CONTACT ?></td>
                            <td>:</td>
                        	<td><b><?php echo $orders[0]->getContactNo();?></b></td>
                        </tr>    
                    	<tr>
                    		<td><?php echo _ORDERS_ADDRESS ?></td>
                            <td>:</td>
                        	<td><b><?php echo $orders[0]->getAddressLine1();?></b></td>
                        </tr> 
                     	<tr>
                    		<td><?php echo _ORDERS_STATUS ?></td>
                            <td>:</td>
                        	<td><b><?php echo $orders[0]->getShippingStatus();?></b></td>
                        </tr>                       
        <?php 
            $payStatus = $orders[0]->getPaymentStatus();
            $shipStatus = $orders[0]->getShippingStatus();
            if($payStatus == "ACCEPTED" && $shipStatus == "DELIVERED")
            {
            ?>
                            
                    	<tr>
                    		<td><?php echo _ORDERS_SHIPPING_METHOD ?></td>
                            <td>:</td>
                        	<td><b><?php echo $orders[0]->getShippingMethod();?></b></td>
                        </tr>    
                    	<tr>
                    		<td><?php echo _ORDERS_TRACKING_NUMBER ?></td>
                            <td>:</td>
                        	<td><b><?php echo $orders[0]->getTrackingNumber();?></b></td>
                        </tr> 
                                
            <?php
            }
            else{}
        ?>                            
                                                                
                    </table>
                </div>
            <?php
            }
            ?>

        </div>


    <div class="clear"></div>

    

</div>
</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function goBack() 
{
    window.history.back();
}
</script>

</body>
</html>