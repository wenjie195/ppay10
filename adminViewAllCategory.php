<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

// $userDetails = getUser($conn," WHERE user_type = 1");
$category = getCategory($conn, "WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Admin View All Category | PPay" />
<title>Admin View All Category | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance">
	<div class="width100">
        <div class="left-h1-div left-user-div">
            <h1 class="green-text h1-title">All Category</h1>
            <div class="green-border"></div>
        </div>
    </div>

    <div class="clear"></div>

	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100">
        	<thead>
            	<tr>
                	<th>No.</th>
                    <th>Category Name</th>
                    <!-- <th>Action</th> -->
                </tr>
            </thead>
            <tbody>
                <?php
                    if($category)
                    {
                        
                        for($cnt = 0;$cnt < count($category) ;$cnt++)
                        {?>
                            
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $category[$cnt]->getCategoryType();?></td>
                                <!-- <td>
                                    <form action="adminEditProduct.php" method="POST" class="hover1">
                                        <button class="clean action-button" type="submit" name="product_uid" value="<?php echo $category[$cnt]->getId();?>">
                                            Edit
                                        </button>
                                    </form> 
                                </td> -->
                            </tr>
                            <?php
                        }
                    }
                ?>                                 
            </tbody>
        </table>
    </div>

    <div class="clear"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Add New Category !";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail To Add New Category !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Category Type Already Existed !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>