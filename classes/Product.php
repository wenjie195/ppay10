<?php  
class Product {
    /* Member variables */
    var $id,$uid,$category,$brand,$name,$sku,$slug,$price,$diamond,$status,$description,$keywordOne,$link,$imageOne,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getDiamond()
    {
        return $this->diamond;
    }

    /**
     * @param mixed $diamond
     */
    public function setDiamond($diamond)
    {
        $this->diamond = $diamond;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getKeywordOne()
    {
        return $this->keywordOne;
    }

    /**
     * @param mixed $keywordOne
     */
    public function setKeywordOne($keywordOne)
    {
        $this->keywordOne = $keywordOne;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getImageOne()
    {
        return $this->imageOne;
    }

    /**
     * @param mixed $imageOne
     */
    public function setImageOne($imageOne)
    {
        $this->imageOne = $imageOne;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getProduct($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","category","brand","name","sku","slug","price","diamond","status","description","keyword_one","link","image_one",
                            "date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"product");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$category,$brand,$name,$sku,$slug,$price,$diamond,$status,$description,$keywordOne,$link,$imageOne,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Product;
            $class->setId($id);
            $class->setUid($uid);
            $class->setCategory($category);
            $class->setBrand($brand);
            $class->setName($name);
            $class->setSku($sku);
            $class->setSlug($slug);
            $class->setPrice($price);
            $class->setDiamond($diamond);
            $class->setStatus($status);
            $class->setDescription($description);
            $class->setKeywordOne($keywordOne);
            $class->setLink($link);
            $class->setImageOne($imageOne);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function createProductList($products,$cartType = 1,$postQuantityRows = null,$isIncludeNotSelectedProductToo = true){
    /*
     * CART TYPE
     * 1 = normal product display cart
     * 2 = checkout cart
     */

    $productListHtml = "";

    if(!$products){
        return $productListHtml;
    }

    $index = 0;
    foreach ($products as $product){
        $quantity = 0;
        if($postQuantityRows){
            $quantity = $postQuantityRows[$index];
        }

        if($quantity <= 0 && !$isIncludeNotSelectedProductToo){
            $productListHtml .= '<div style="display: none;">';
        }else{
            $productListHtml .= '<div style="display: block;">';
        }

        $conn=connDB();
        //$productArray = getProduct($conn);
        $id  = $product->getName();
        // Include the database configuration file

        // Get images from the database
        $query = $conn->query("SELECT image_one FROM product WHERE name = '$id'");

        if($query->num_rows > 0){
            while($row = $query->fetch_assoc()){
                $imageURL = './uploads/'.$row["image_one"];

                    $productListHtml .= '
                        <!-- Product -->
                        <div class="shadow-white-box four-box-size ow-product-big-div opacity-hover">
                           <div class="square2">
							<div class="width100 white-bg content2">
                                <img src="'.$imageURL.'" alt="'.$product->getName().'" title="'.$product->getName().'" class="width100">
                            </div>
						  </div>
                            <div class="width100 product-details-div">
                                <p class="width100 text-overflow slider-product-name ">'.$product->getName().'</p>
                                <p class="slider-product-price text-overflow text-center">RM '.$product->getPrice().' / '.$product->getDiamond().' Diamond</p>
                                <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">

                                <div class="input-group div-center">
                                    <button type="button" class="button-minus math-button clean">-</button>
                                        <input readonly type="number" step="1" max="" value="'.$quantity.'" name="product-list-quantity-input['.$index.']" class="quantity-field math-input clean">
                                    <button type="button" class="button-plus math-button clean">+</button>
                                </div>
                            </div>
                        </div>      
                    </div>

                    ';
            }
        }
        $index++;
    }

    $productListHtml .= '
    <script>
        $(".button-minus").on("click", function(e)
        {
            e.preventDefault();
            var $this = $(this);
            var $input = $this.closest("div").find("input");
            var value = parseInt($input.val());
            if (value > 1)
            {
                value = value - 1;
            } 
            else 
            {
                value = 0;
            }
            $input.val(value);
        });

        $(".button-plus").on("click", function(e)
        {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value < 100)
        {
            value = value + 1;
        }
        else
        {
            value = 100;
        }
        $input.val(value);
        });
    </script>
    ';

    return $productListHtml;
}

function getProductPrice($conn,$productId){
    $price = 0;

    $productRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");

    if($productRows){
        $price = $productRows[0]->getDiamond();
    }

    return $price;
}

function addToCart(){
    $shoppingCart = array();

    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

    //    if($quantity > 0){
        $thisOrder = array();
        $thisOrder['productId'] = $productId;
        $thisOrder['quantity'] = $quantity;
        array_push($shoppingCart,$thisOrder);
    //    }
    }

    if(count($shoppingCart) > 0) {
        $_SESSION['shoppingCart'] = $shoppingCart;
    }

}

function clearCart(){
    unset ($_SESSION["shoppingCart"]);
}

//when checkout only create order
function createOrder($conn,$uid){
    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'] && count($_SESSION['shoppingCart']) > 0){
        $shoppingCart = $_SESSION['shoppingCart'];

        $order_Id = md5(uniqid());

        $orderId = insertDynamicData($conn,"orders",array("uid","order_id"),array($uid,$order_Id),"ss");
        //$orderId = insertDynamicData($conn,"orders",array("uid","username","full_name"),array($uid,$username,$fullName),"sss");

        if($orderId){
            $totalPrice = 0;

            for($index = 0; $index < count($shoppingCart); $index++){
                $thisCart = $shoppingCart[$index];
                $productId = $thisCart['productId'];
                $quantity = $thisCart['quantity'];
                $originalPrice = getProductPrice($conn,$productId);
                $totalPrice += ($originalPrice * $quantity);

                if(!insertDynamicData($conn,"product_orders",array("product_id","order_id","quantity","final_price","original_price","discount_given"),
                    array($productId,$orderId,$quantity,$originalPrice,$originalPrice,0),"iiiddd")){
                    promptError("error creating order for product : $productId");
                }
            }

        }else{
            promptError("error creating order");
        }

    }
}

function getShoppingCart($conn,$cartType = 2,$isIncludeNotSelectedProductToo = false){
    $productListHtml = "";

    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
        $products = array();
        $quantities = array();
        for($index = 0; $index < count($_SESSION['shoppingCart']); $index++){
            $thisCart = $_SESSION['shoppingCart'][$index];

            $tempProductRows = getProduct($conn," WHERE id = ? ",array("id"),array($thisCart['productId']),"i");
            if($tempProductRows){
                array_push($products,$tempProductRows[0]);
                array_push($quantities,$thisCart['quantity']);
            }
        }

        if(count($products) > 0 && count($quantities) > 0 && count($products) == count($quantities)){
            $productListHtml = createProductList($products,$cartType,$quantities,$isIncludeNotSelectedProductToo);
        }
    }

    return $productListHtml;
}
