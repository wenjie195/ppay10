<?php
class OldOrders {
    /* Member variables */
    var $id,$userId,$cart,$method,$shipping,$pickupLocation,$totalQty,$payAmount,$txnId,$chargeId,$orderNumber,$paymentStatus,$cusEmail,$cusName,$cusCountry,$cusPhone,
        $cusAddress,$cusCity,$cusZip,$shipName,$shipCountry,$shipEmail,$shipPhone,$shipAddress,$shipCity,$shipZip,$orderNote,$couponCode,$couponDiscount,$status,
        $createdAt,$updatedAt,$affilateUser,$affilateCharge,$currencySign,$currencyValue,$shippingCost,$packingCost,$tax,$dp,$payId,$vendorShippingId,$vendorPackingId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param mixed $cart
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * @param mixed $shipping
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * @return mixed
     */
    public function getPickupLocation()
    {
        return $this->pickupLocation;
    }

    /**
     * @param mixed $pickupLocation
     */
    public function setPickupLocation($pickupLocation)
    {
        $this->pickupLocation = $pickupLocation;
    }

    /**
     * @return mixed
     */
    public function getTotalQty()
    {
        return $this->totalQty;
    }

    /**
     * @param mixed $totalQty
     */
    public function setTotalQty($totalQty)
    {
        $this->totalQty = $totalQty;
    }

    /**
     * @return mixed
     */
    public function getPayAmount()
    {
        return $this->payAmount;
    }

    /**
     * @param mixed $payAmount
     */
    public function setPayAmount($payAmount)
    {
        $this->payAmount = $payAmount;
    }

    /**
     * @return mixed
     */
    public function getTxnId()
    {
        return $this->txnId;
    }

    /**
     * @param mixed $txnId
     */
    public function setTxnId($txnId)
    {
        $this->txnId = $txnId;
    }

    /**
     * @return mixed
     */
    public function getChargeId()
    {
        return $this->chargeId;
    }

    /**
     * @param mixed $chargeId
     */
    public function setChargeId($chargeId)
    {
        $this->chargeId = $chargeId;
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param mixed $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return mixed
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * @param mixed $paymentStatus
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;
    }

    /**
     * @return mixed
     */
    public function getCusEmail()
    {
        return $this->cusEmail;
    }

    /**
     * @param mixed $cusEmail
     */
    public function setCusEmail($cusEmail)
    {
        $this->cusEmail = $cusEmail;
    }

    /**
     * @return mixed
     */
    public function getCusName()
    {
        return $this->cusName;
    }

    /**
     * @param mixed $cusName
     */
    public function setCusName($cusName)
    {
        $this->cusName = $cusName;
    }

    /**
     * @return mixed
     */
    public function getCusCountry()
    {
        return $this->cusCountry;
    }

    /**
     * @param mixed $cusCountry
     */
    public function setCusCountry($cusCountry)
    {
        $this->cusCountry = $cusCountry;
    }

    /**
     * @return mixed
     */
    public function getCusPhone()
    {
        return $this->cusPhone;
    }

    /**
     * @param mixed $cusPhone
     */
    public function setCusPhone($cusPhone)
    {
        $this->cusPhone = $cusPhone;
    }

    /**
     * @return mixed
     */
    public function getCusAddress()
    {
        return $this->cusAddress;
    }

    /**
     * @param mixed $cusAddress
     */
    public function setCusAddress($cusAddress)
    {
        $this->cusAddress = $cusAddress;
    }

    /**
     * @return mixed
     */
    public function getCusCity()
    {
        return $this->cusCity;
    }

    /**
     * @param mixed $cusCity
     */
    public function setCusCity($cusCity)
    {
        $this->cusCity = $cusCity;
    }

    /**
     * @return mixed
     */
    public function getCusZip()
    {
        return $this->cusZip;
    }

    /**
     * @param mixed $cusZip
     */
    public function setCusZip($cusZip)
    {
        $this->cusZip = $cusZip;
    }

    /**
     * @return mixed
     */
    public function getShipName()
    {
        return $this->shipName;
    }

    /**
     * @param mixed $shipName
     */
    public function setShipName($shipName)
    {
        $this->shipName = $shipName;
    }

    /**
     * @return mixed
     */
    public function getShipCountry()
    {
        return $this->shipCountry;
    }

    /**
     * @param mixed $shipCountry
     */
    public function setShipCountry($shipCountry)
    {
        $this->shipCountry = $shipCountry;
    }

    /**
     * @return mixed
     */
    public function getShipEmail()
    {
        return $this->shipEmail;
    }

    /**
     * @param mixed $shipEmail
     */
    public function setShipEmail($shipEmail)
    {
        $this->shipEmail = $shipEmail;
    }

    /**
     * @return mixed
     */
    public function getShipPhone()
    {
        return $this->shipPhone;
    }

    /**
     * @param mixed $shipPhone
     */
    public function setShipPhone($shipPhone)
    {
        $this->shipPhone = $shipPhone;
    }

    /**
     * @return mixed
     */
    public function getShipAddress()
    {
        return $this->shipAddress;
    }

    /**
     * @param mixed $shipAddress
     */
    public function setShipAddress($shipAddress)
    {
        $this->shipAddress = $shipAddress;
    }

    /**
     * @return mixed
     */
    public function getShipCity()
    {
        return $this->shipCity;
    }

    /**
     * @param mixed $shipCity
     */
    public function setShipCity($shipCity)
    {
        $this->shipCity = $shipCity;
    }

    /**
     * @return mixed
     */
    public function getShipZip()
    {
        return $this->shipZip;
    }

    /**
     * @param mixed $shipZip
     */
    public function setShipZip($shipZip)
    {
        $this->shipZip = $shipZip;
    }

    /**
     * @return mixed
     */
    public function getOrderNote()
    {
        return $this->orderNote;
    }

    /**
     * @param mixed $orderNote
     */
    public function setOrderNote($orderNote)
    {
        $this->orderNote = $orderNote;
    }

    /**
     * @return mixed
     */
    public function getCouponCode()
    {
        return $this->couponCode;
    }

    /**
     * @param mixed $couponCode
     */
    public function setCouponCode($couponCode)
    {
        $this->couponCode = $couponCode;
    }

    /**
     * @return mixed
     */
    public function getCouponDiscount()
    {
        return $this->couponDiscount;
    }

    /**
     * @param mixed $couponDiscount
     */
    public function setCouponDiscount($couponDiscount)
    {
        $this->couponDiscount = $couponDiscount;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getAffilateUser()
    {
        return $this->affilateUser;
    }

    /**
     * @param mixed $affilateUser
     */
    public function setAffilateUser($affilateUser)
    {
        $this->affilateUser = $affilateUser;
    }

    /**
     * @return mixed
     */
    public function getAffilateCharge()
    {
        return $this->affilateCharge;
    }

    /**
     * @param mixed $affilateCharge
     */
    public function setAffilateCharge($affilateCharge)
    {
        $this->affilateCharge = $affilateCharge;
    }

    /**
     * @return mixed
     */
    public function getCurrencySign()
    {
        return $this->currencySign;
    }

    /**
     * @param mixed $currencySign
     */
    public function setCurrencySign($currencySign)
    {
        $this->currencySign = $currencySign;
    }

    /**
     * @return mixed
     */
    public function getCurrencyValue()
    {
        return $this->currencyValue;
    }

    /**
     * @param mixed $currencyValue
     */
    public function setCurrencyValue($currencyValue)
    {
        $this->currencyValue = $currencyValue;
    }

    /**
     * @return mixed
     */
    public function getShippingCost()
    {
        return $this->shippingCost;
    }

    /**
     * @param mixed $shippingCost
     */
    public function setShippingCost($shippingCost)
    {
        $this->shippingCost = $shippingCost;
    }

    /**
     * @return mixed
     */
    public function getPackingCost()
    {
        return $this->packingCost;
    }

    /**
     * @param mixed $packingCost
     */
    public function setPackingCost($packingCost)
    {
        $this->packingCost = $packingCost;
    }

    /**
     * @return mixed
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param mixed $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * @return mixed
     */
    public function getDp()
    {
        return $this->dp;
    }

    /**
     * @param mixed $dp
     */
    public function setDp($dp)
    {
        $this->dp = $dp;
    }

    /**
     * @return mixed
     */
    public function getPayId()
    {
        return $this->payId;
    }

    /**
     * @param mixed $payId
     */
    public function setPayId($payId)
    {
        $this->payId = $payId;
    }

    /**
     * @return mixed
     */
    public function getVendorShippingId()
    {
        return $this->vendorShippingId;
    }

    /**
     * @param mixed $vendorShippingId
     */
    public function setVendorShippingId($vendorShippingId)
    {
        $this->vendorShippingId = $vendorShippingId;
    }

    /**
     * @return mixed
     */
    public function getVendorPackingId()
    {
        return $this->vendorPackingId;
    }

    /**
     * @param mixed $vendorPackingId
     */
    public function setVendorPackingId($vendorPackingId)
    {
        $this->vendorPackingId = $vendorPackingId;
    }

}

function getOldOrders($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","user_id","cart","method","shipping","pickup_location","totalQty","pay_amount","txnid","charge_id","order_number","payment_status",
                    "customer_email","customer_name","customer_country","customer_phone","customer_address","customer_city","customer_zip","shipping_name","shipping_country",
                    "shipping_email","shipping_phone","shipping_address","shipping_city","shipping_zip","order_note","coupon_code","coupon_discount","status","created_at",
                    "updated_at","affilate_user","affilate_charge","currency_sign","currency_value","shipping_cost","packing_cost","tax","dp","pay_id",
                    "vendor_shipping_id","vendor_packing_id");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"old_orders");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$userId,$cart,$method,$shipping,$pickupLocation,$totalQty,$payAmount,$txnId,$chargeId,$orderNumber,$paymentStatus,$cusEmail,$cusName,
                $cusCountry,$cusPhone,$cusAddress,$cusCity,$cusZip,$shipName,$shipCountry,$shipEmail,$shipPhone,$shipAddress,$shipCity,$shipZip,$orderNote,$couponCode,
                $couponDiscount,$status,$createdAt,$updatedAt,$affilateUser,$affilateCharge,$currencySign,$currencyValue,$shippingCost,$packingCost,$tax,$dp,$payId,
                $vendorShippingId,$vendorPackingId);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new OldOrders();

            $class->setId($id);
            $class->setUserId($userId);
            $class->setCart($userId);
            $class->setMethod($method);
            $class->setShipping($shipping);
            $class->setPickupLocation($pickupLocation);
            $class->setTotalQty($totalQty);
            $class->setPayAmount($payAmount);
            $class->setTxnId($txnId);
            $class->setChargeId($chargeId);
            $class->setOrderNumber($orderNumber);
            $class->setPaymentStatus($paymentStatus);
            $class->setCusEmail($cusEmail);
            $class->setCusName($cusName);
            $class->setCusCountry($cusCountry);
            $class->setCusPhone($cusPhone);
            $class->setCusAddress($cusAddress);
            $class->setCusCity($cusCity);
            $class->setCusZip($cusZip);
            $class->setShipName($shipName);
            $class->setShipCountry($shipCountry);
            $class->setShipEmail($shipEmail);
            $class->setShipPhone($shipPhone);
            $class->setShipAddress($shipAddress);
            $class->setShipCity($shipCity);
            $class->setShipZip($shipZip);
            $class->setOrderNote($orderNote);
            $class->setCouponCode($couponCode);
            $class->setCouponDiscount($couponDiscount);
            $class->setStatus($status);
            $class->setCreatedAt($createdAt);
            $class->setUpdatedAt($updatedAt);
            $class->setAffilateUser($affilateUser);
            $class->setAffilateCharge($affilateCharge);
            $class->setCurrencySign($currencySign);
            $class->setCurrencyValue($currencyValue);
            $class->setShippingCost($shippingCost);
            $class->setPackingCost($packingCost);
            $class->setTax($tax);
            $class->setDp($dp);
            $class->setPayId($payId);
            $class->setVendorShippingId($vendorShippingId);
            $class->setVendorPackingId($vendorPackingId);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}