<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$userId = $_SESSION['user_id'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_id =?",array("user_id"),array($userId),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_ADD_NEW_CATEGORY2 ?> | PPay" />
<title><?php echo _ADMIN_ADD_NEW_CATEGORY2 ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay,e-commerce,iphone,phone,huawei">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
  <div class="width100">
    <h1 class="green-text h1-title"><?php echo _ADMIN_ADD_NEW_CATEGORY2 ?></h1>
    <div class="green-border"></div>
  </div>

  <div class="clear"></div>

  <form action="utilities/addNewCategoryFunction.php" method="POST" enctype="multipart/form-data">
    <div class="border-separation">
      <div class="width100">
        <p class="input-top-p admin-top-p"><?php echo _ADMIN_CATEGORY_NAME ?></p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="<?php echo _ADMIN_CATEGORY_NAME ?>" name="register_category" id="register_category" required>
      </div>
      <div class="clear"></div>
      <div class="width100 overflow text-center">
        <button class="green-button white-text clean2 edit-1-btn margin-auto" name="submit"><?php echo _ADMIN_NEXT ?></button>
      </div>
    </div>
  </form>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Add New Category!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail To Add New Category!!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Category Type Already Existed!!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>