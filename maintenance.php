<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
// require_once dirname(__FILE__) . '/classes/Variation.php';
require_once dirname(__FILE__) . '/classes/Slider.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$slider = getSlider($conn," WHERE status = 'Show' ");
$products = getProduct($conn, "WHERE status = 'Available' ");
// $products = getProduct($conn, "WHERE featured_product = 'Yes' ORDER BY date_created DESC LIMIT 10 ");
// $variationDetails = getVariation($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Maintenance | PPay" />
<title>Maintenance | PPay</title>
<meta property="og:description" content="The website is under maintenance now, sorry for any inconvenience." />
<meta name="description" content="The website is under maintenance now, sorry for any inconvenience." />
<meta name="keywords" content="PPay,e-commerce,iphone,phone,huawei">

 <link rel="stylesheet" type="text/css" href="css/glider.css">
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'header.php'; ?>
<div class="min-height overflow width100 small-padding">
	<p class="text-center"><img src="img/maintenance.jpg" class="maintenance-jpg" alt="Maintenance" title="Maintenance"></p>
	<p class="maintenance-text">网站正在维修，任何不便请见谅。<br>The website is under maintenance now, sorry for any inconvenience.</p>
</div>

<div class="clear"></div>


<?php include 'js.php'; ?>



<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Login Succesfully !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Unknown User, Please contact admin !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Wrong Password";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Please register !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>