<?php
//Start the session
if (session_id() == "")
{
    session_start();
}
//Check if the session uid is empty/exist or not
if(empty($_SESSION['user_id']))
{
    // Go back to index page
    // NOTE : MUST PROMPT ERROR
    header('Location:index.php');
    exit();
}
else
{
    $uid = $_SESSION['user_id'];
}

if (isset($_GET['lang']) == 'en')
{
  $_SESSION['lang'] = 'en';
}
elseif (isset($_GET['lang']) == 'ch')
{
  $_SESSION['lang'] = 'ch';
}