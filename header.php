<?php
if(isset($_SESSION['user_id']))
{
?>
    <?php
    if($_SESSION['usertype'] == 0)
    {
    ?>
        <header id="header" class="header header--fixed same-padding header1 menu-color admin-menu" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/ppay-white.png" class="logo-img opacity-hover" alt="PPay" title="PPay"></a>
                    </div>
                    <div class="right-menu-div float-right">
                        <a href="adminDashboard.php" class="menu-padding black-to-white text-menu-a"><?php echo _ADMIN_DASHBOARD ?></a>                     
                        <div class="dropdown hover1">
                            <a  class="menu-padding black-to-white menu-margin text-menu-a">
                                <?php echo _ADMIN_USER ?> <img src="img/dropdown.png" class="menu-dropdown-img" >
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">

                                <p class="dropdown-p"><a href="adminAllUsers.php"  class="menu-padding dropdown-a"><?php echo _ADMIN_ALL_USERS ?></a></p>
     							<p class="dropdown-p"><a href="adminAddMember.php"  class="menu-padding dropdown-a"><?php echo _ADMIN_ADD_NEW_USER ?></a></p>
                                <p class="dropdown-p"><a href="adminTopUpUserPoints.php"  class="menu-padding dropdown-a"><?php echo _ADMIN_TOP_UP_USER_POINTS ?> (+)</a></p>
                                <p class="dropdown-p"><a href="adminAddUserPoints.php"  class="menu-padding dropdown-a"><?php echo _ADMIN_RENEW_USER_POINTS ?></a></p> 
                                <p class="dropdown-p"><a href="exportExcel.php"  class="menu-padding dropdown-a">Export Data</a></p> 
                            </div>
                        </div>                        

                        <a href="adminViewAllProduct.php" class="menu-padding black-to-white menu-margin text-menu-a"><?php echo _PRODUCT_ALL ?></a>
                        <a href="adminViewPointsConversion.php" class="menu-padding black-to-white menu-margin text-menu-a"><?php echo _PRODUCT_POINTS_CONVERSION ?></a>

                        <div class="dropdown hover1">
                            <a  class="menu-padding black-to-white menu-margin text-menu-a">
                                <?php echo _INDEX_ORDER ?> <img src="img/dropdown.png" class="menu-dropdown-img" >
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                <!-- <p class="dropdown-p"><a href="#"  class="menu-padding dropdown-a">Pending Orders</a></p> -->
                                <p class="dropdown-p"><a href="adminViewPendingOrders.php"  class="menu-padding dropdown-a"><?php echo _ORDERS_PENDING ?></a></p>
                                <p class="dropdown-p"><a href="adminViewApprovedOrders.php"  class="menu-padding dropdown-a"><?php echo _ORDERS_APPROVED ?></a></p>
                                <p class="dropdown-p"><a href="adminViewRejectedOrders.php"  class="menu-padding dropdown-a"><?php echo _ORDERS_REJECTED ?></a></p>
                            </div>
                        </div>   


                        <a href="logout.php"  class="enu-padding black-to-white menu-margin text-menu-a"><?php echo _MAINJS_ALL_LOGOUT ?></a>    
                <div class="dropdown hover1 text-menu-a">
                    <a  class="menu-padding black-to-white menu-margin text-menu-a">
                        Language / 语言 <img src="img/dropdown.png" class="menu-dropdown-img" alt="Language / 语言" title="Language / 语言">
                    </a>
                    <div class="dropdown-content yellow-dropdown-content menu-item">
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a">中文</a></p>
                    </div>
                </div>                         
                        
                        
                                          
                            <div id="dl-menu" class="dl-menuwrapper admin-dl-menu">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                        <li><a href="adminDashboard.php"><?php echo _ADMIN_DASHBOARD ?></a></li>
                                        <!--<li><a href="homeSettings.php">Homepage Settings</a></li>-->
                                        <li><a href="adminAllUsers.php"  class="mini-li"><?php echo _ADMIN_ALL_USERS ?></a></li>  
                                        <li><a href="exportExcel.php"  class="mini-li">Export Data</a></li>  
                                        <li><a href="adminViewAllProduct.php"  class="mini-li"><?php echo _PRODUCT_ALL ?></a></li>  
                                        <li><a href="adminViewPointsConversion.php"  class="mini-li"><?php echo _PRODUCT_POINTS_CONVERSION ?></a></li>  
                                        
                                        <li><a href="adminViewPendingOrders.php" class="mini-li"><?php echo _ORDERS_PENDING ?></a></li>
                                        <li><a href="adminViewApprovedOrders.php" class="mini-li"><?php echo _ORDERS_APPROVED ?></a></li>
                                        <li><a href="adminViewRejectedOrders.php" class="mini-li"><?php echo _ORDERS_REJECTED ?></a></li>

                                        <li><a href="adminAddMember.php" class="mini-li"><?php echo _ADMIN_ADD_NEW_USER ?></a></li>
                                        <li><a href="adminTopUpUserPoints.php"  class="mini-li"><?php echo _ADMIN_TOP_UP_USER_POINTS ?> (+)</a></li>     
                                        <li><a href="adminAddUserPoints.php" class="mini-li"><?php echo _ADMIN_RENEW_USER_POINTS ?></a></li>                                                             
                                        <li  class="last-li"><a href="logout.php"><?php echo _MAINJS_ALL_LOGOUT ?></a></li>
                                </ul>
                            </div>                
                    </div>
                </div>

        </header>

    <?php
    }
    elseif($_SESSION['usertype'] == 1)
    {
    ?>
        <header id="header" class="header header--fixed same-padding header1 menu-color logged-menu" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"><img src="img/ppay-white.png" class="logo-img opacity-hover" alt="PPay" title="PPay"></a>
                    </div>
                    <div class="right-menu-div float-right text-menu-right">
                        <a href="index.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            <?php echo _INDEX_HOME ?>
                        </a>   
                        <a href="profile.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            <?php echo _INDEX_PROFILE ?>
                        </a> 

                        <div class="dropdown hover1">
                            <a  class="menu-padding black-to-white menu-margin text-menu-a">
                                <?php echo _INDEX_ORDER ?> <img src="img/dropdown.png" class="menu-dropdown-img" >
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="userViewPendingOrders.php"  class="menu-padding dropdown-a"><?php echo _INDEX_PENDING ?></a></p>
                                <p class="dropdown-p"><a href="userViewApprovedOrders.php"  class="menu-padding dropdown-a"><?php echo _INDEX_APPROVED ?></a></p>
                                <p class="dropdown-p"><a href="userViewRejectedOrders.php"  class="menu-padding dropdown-a"><?php echo _INDEX_REJECTED ?></a></p>
                            </div>
                        </div>  

                        <a href="product.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            <?php echo _INDEX_PRODUCT ?>
                        </a> 
                        <a href="logout.php"  class="menu-padding black-to-white menu-margin text-menu-a">
                            <?php echo _MAINJS_ALL_LOGOUT ?>
                        </a>
                <div class="dropdown hover1 text-menu-a">
                    <a  class="menu-padding black-to-white menu-margin text-menu-a">
                        Language / 语言 <img src="img/dropdown.png" class="menu-dropdown-img" alt="Language / 语言" title="Language / 语言">
                    </a>
                    <div class="dropdown-content yellow-dropdown-content menu-item">
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a">中文</a></p>
                    </div>
                </div>                                                                                            
                            <div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                    <li><a href="index.php" class="mini-li"><?php echo _INDEX_HOME ?></a></li>
                                    <li><a href="profile.php" class="mini-li"><?php echo _INDEX_PROFILE ?></a></li>
                                    <li><a href="userViewPendingOrders.php" class="mini-li"><?php echo _INDEX_PENDING ?></a></li>
                                    <li><a href="userViewApprovedOrders.php" class="mini-li"><?php echo _INDEX_APPROVED ?></a></li>
                                    <li><a href="userViewRejectedOrders.php" class="mini-li"><?php echo _INDEX_REJECTED ?></a></li>
                                    <li><a href="product.php" class="mini-li"><?php echo _INDEX_PRODUCT ?></a></li>
                                    <li><a href="logout.php" class="mini-li"><?php echo _MAINJS_ALL_LOGOUT ?></a></li>
                                    <li><a href="<?php $link ?>?lang=en" class="mini-li">English</a></li>
                                    <li><a href="<?php $link ?>?lang=ch" class="mini-li">中文</a></li>                                    
                                </ul>
                            </div>                              
                    </div>
                </div>
        </header>
    <?php
    }
    ?>
<?php
}
else
{
?>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v6.0&appId=2101026363514817&autoLogAppEvents=1"></script>
    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
            <div class="big-container-size hidden-padding" id="top-menu">
                <div class="float-left left-logo-div">
                    <a href="index.php"><img src="img/ppay-white.png" class="logo-img opacity-hover" alt="PPay" title="PPay"></a>
                </div>



                <div class="right-menu-div float-right text-menu-right">

                    <a href="index.php" class="menu-padding black-to-white text-menu-a">
                        <?php echo _INDEX_HOME ?>
                    </a>                           
                    <!-- <a class="menu-padding black-to-white open-signup menu-margin text-menu-a">
                        Sign Up
                    </a> -->
                    <a href="product-open.php" class="menu-padding black-to-white menu-margin text-menu-a">
                        <?php echo _INDEX_PRODUCT ?>
                    </a>   
                    <a  class="menu-padding black-to-white open-login menu-margin text-menu-a">
                        <?php echo _MAINJS_INDEX_LOGIN ?>
                    </a>  
                <div class="dropdown hover1 text-menu-a">
                    <a  class="menu-padding black-to-white menu-margin text-menu-a">
                        Language / 语言 <img src="img/dropdown.png" class="menu-dropdown-img" alt="Language / 语言" title="Language / 语言">
                    </a>
                    <div class="dropdown-content yellow-dropdown-content menu-item">
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a">中文</a></p>
                    </div>
                </div>                     
                    <div id="dl-menu" class="dl-menuwrapper">
                        <button class="dl-trigger">Open Menu</button>
                        <ul class="dl-menu">
                                <!-- <li><a class="open-signup mini-li">Sign Up</a></li> -->
                                <li><a href="index.php" class="mini-li"><?php echo _INDEX_HOME ?></a></li>
                                <li><a href="product-open.php" class="mini-li"><?php echo _INDEX_PRODUCT ?></a></li>
                                <li><a class="open-login mini-li"><?php echo _MAINJS_INDEX_LOGIN ?></a></li>
                                <li><a href="<?php $link ?>?lang=en" class="mini-li">English</a></li>
                                <li><a href="<?php $link ?>?lang=ch" class="mini-li">中文</a></li>
                        </ul>
                    </div>   

                </div>
            </div>
    </header>
<?php
}
?>