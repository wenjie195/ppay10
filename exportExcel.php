<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

include("export_data.php");
include 'selectFilecss.php';

$conn = connDB();

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Export Data | PPay" />
<title>Export Data | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance">
	<div class="width100">
        <div class="left-h1-div left-user-div">
            <h1>Export Data</h1>
        </div>
    </div>

    <div class="clear"></div>

    <?php 
    // $query = "SELECT count(*) AS total FROM `userdata`";

    $query = "SELECT count(*) AS total FROM `users`";

    $result = mysqli_query($conn, $query);
    while ($row = mysqli_fetch_array($result))
    {
    $row['total'];
    ?>
    <h2>Total User : <?php echo $row['total'] ?></h2>
    <?php
    }
    ?>

    <div class="buttons">
      <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
      <button type="submit" id="export_data" name='export_data' value="Export to excel" class="btn-hover color-10">Download Data</button>
      <!-- <a href="adminAllUsers.php"><button type="button" class="btn-hover color-8">Cancel / Return</button></a> -->
      </form>
    </div>

    </div>

    <div class="clear"></div>

    <div class="width100 bottom-spacing"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>