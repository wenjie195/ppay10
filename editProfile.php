<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$userId = $_SESSION['user_id'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_id =?",array("user_id"),array($userId),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _EDITPRO_TITLE ?> | PPay" />
<title><?php echo _EDITPRO_TITLE ?> | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="width100 same-padding overflow min-height2 menu-distance">
        <!-- <p class="review-product-name">Edit Profile</p> -->
        <p class="review-product-name"><?php echo _EDITPRO_TITLE ?></p>
 		<!-- <form> -->
        <form method="POST" action="utilities/editProfileFunction.php">
            <!-- <div class="dual-input">
                <p class="input-top-p">Username</p>
                <input class="input-name clean input-textarea" type="text" placeholder="Name" value="<?php //echo $userData->getUsername();?>" name="update_username" id="update_username" readonly>   
            </div> -->

            <div class="dual-input">
                <!-- <p class="input-top-p">Name</p> -->
                <p class="input-top-p"><?php echo _EDITPRO_NAME ?></p>
                <input class="input-name clean input-textarea" type="text" placeholder="<?php echo _EDITPRO_NAME ?>" value="<?php echo $userData->getName();?>" name="update_name" id="update_name" required>   
            </div>

            <div class="dual-input second-dual-input">
                <!-- <p class="input-top-p">Phone No.</p> -->
                <p class="input-top-p"><?php echo _EDITPRO_PHONE ?></p>
                <input class="input-name clean" type="text" placeholder="<?php echo _EDITPRO_PHONE ?>" value="<?php echo $userData->getPhone();?>" id="update_phone" name="update_phone" required>   
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <!-- <p class="input-top-p">Email</p> -->
                <p class="input-top-p"><?php echo _EDITPRO_EMAIL ?></p>
                <input class="input-name clean" type="email" placeholder="<?php echo _EDITPRO_EMAIL ?>" value="<?php echo $userData->getEmail();?>" id="update_email" name="update_email" required> 
            </div>
        
            <div class="dual-input second-dual-input">
                <!-- <p class="input-top-p">Address</p> -->
                <p class="input-top-p"><?php echo _EDITPRO_ADDRESS ?></p>
                <input class="input-name clean" type="text" placeholder="<?php echo _EDITPRO_ADDRESS ?>" value="<?php echo $userData->getAddress();?>" id="update_address" name="update_address" required>   
            </div>

            <div class="clear"></div>

            <div class="width100 overflow text-center">     
                <!-- <button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button> -->
                <button class="green-button white-text clean2 edit-1-btn margin-auto"><?php echo _EDITPRO_SUBMIT ?></button>
            </div>
            <div class="width100 overflow text-center go-back">
            	<a onclick="goBack()" class="green-a"><?php echo _USER_BACK ?></a>
            </div>
        </form>
    </div>
    
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Profile Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update profile";
        }
        else if($_GET['type'] == 3)
        {
            // $messageType = "User details is not available !!";
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>