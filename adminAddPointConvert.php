<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$timestamp = time();

include 'selectFilecss.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
  if(in_array($_FILES["file"]["type"],$allowedFileType))
  {
    $targetPath = 'uploadsExcel/'.$timestamp.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    $Reader = new SpreadsheetReader($targetPath);
    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {
      $Reader->ChangeSheet($i);
      foreach ($Reader as $Row)
      {

        $user_id = "";
        if(isset($Row[0]))
        {
          $user_id = mysqli_real_escape_string($conn,$Row[0]);
        }
        $username = "";
        if(isset($Row[0]))
        {
          $username = mysqli_real_escape_string($conn,$Row[1]);
        }
        $currentPoints = "";
        if(isset($Row[0]))
        {
          $currentPoints = mysqli_real_escape_string($conn,$Row[2]);
        }
        $convertPoints = "";
        if(isset($Row[0]))
        {
          $convertPoints = mysqli_real_escape_string($conn,$Row[3]);
        }
        $currentDiamond = "";
        if(isset($Row[0]))
        {
          $currentDiamond = mysqli_real_escape_string($conn,$Row[4]);
        }
        $convertDiamond = "";
        if(isset($Row[0]))
        {
          $convertDiamond = mysqli_real_escape_string($conn,$Row[5]);
        }

        $uid = md5(uniqid());

        if (!empty($user_id) || !empty($username) || !empty($currentPoints) || !empty($convertPoints) || !empty($currentDiamond) || !empty($convertDiamond)  )
        {
          $query = "INSERT INTO convert_point (uid,user_id,user_name,current_points,convert_points,current_diamond,diamond) 
                      VALUES ('".$uid."','".$user_id."','".$username."','".$currentPoints."','".$convertPoints."','".$currentDiamond."','".$convertDiamond."') ";
          $result = mysqli_query($conn, $query);
          if (! empty($result))
          {
            echo "<script>alert('Excel File Uploaded !!');window.location='../ppay10/adminAddPointConvert.php'</script>";
          }
          else 
          {
            echo "<script>alert('Fail to upload excel file !!');window.location='../ppay10/adminAddPointConvert.php'</script>";
          }
        }
      }
    }
  }
  else
  {
    echo "<script>alert('Invalid File Type. Upload Excel File.');window.location='../ppay10/adminAddPointConvert.php'</script>";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <?php include 'meta.php'; ?>
  <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
  <meta property="og:title" content="Add New Member (Import Excel) | PPay" />
  <title>Add New Member (Import Excel) | PPay</title>
  <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
  <?php include 'css.php'; ?>
</head>


<body class="body">

<?php include 'header.php'; ?>
<div class="next-to-sidebar"> 

  <div class="width100 same-padding menu-distance min-height2 text-center">
  	<h1 class="h1-title green-text">Add Previous Convert Points to Diamond (Import Excel)</h1>
    <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
     <input type="file" name="file" id="file" class="choose-file-input" accept=".xls,.xlsx"><div class="clear"></div>
      <button type="submit" id="submit" name="import"  class="clean blue-gradient-button">Submit</button>
      <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    </form>
  </div>

<style>
.import-li{
color:#bf1b37;
background-color:white;}
.import-li .hover1a{
display:none;}
.import-li .hover1b{
display:block;}
</style>

<?php include 'js.php'; ?>

<script  src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
    </script>

</body>
</html>