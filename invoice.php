<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['user_id'];

$conn = connDB();
$date = date("Y-m-d");
$time = date("h:i a");

$id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$orderUid = $id[0]->getId();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = connDB();
    
    $uid = ($_POST['uid']);
    $userId = ($_POST['userId']);
   
    $subtotal = rewrite($_POST["subtotal"]);
    $total = rewrite($_POST["total"]);

    $payment_method = rewrite($_POST["payment_method"]);
    $payment_status = rewrite($_POST["payment_status"]);
    $shipping_status = rewrite($_POST["shipping_status"]);

    $totalDiamond = rewrite($_POST["total_diamond"]);
    $onhandDiamond = rewrite($_POST["onhand_diamond"]);

    $balanceDiamond = $onhandDiamond - $totalDiamond;

    if(isset($_POST['next']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
    
        //echo "save to database";
        if($subtotal)
        {
            array_push($tableName,"subtotal");
            array_push($tableValue,$subtotal);
            $stringType .=  "d";
        }
        if($total)
        {
            array_push($tableName,"total");
            array_push($tableValue,$total);
            $stringType .=  "d";
        }
        if($payment_method)
        {
            array_push($tableName,"payment_method");
            array_push($tableValue,$payment_method);
            $stringType .=  "s";
        }
        
        if($payment_status)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$payment_status);
            $stringType .=  "s";
        }
        if($shipping_status)
        {
            array_push($tableName,"shipping_status");
            array_push($tableValue,$shipping_status);
            $stringType .=  "s";
        }

        // echo $uid. "<br>";
        //echo $userId. "<br>";
        // echo $subtotal. "<br>";
        // echo $total. "<br>";
        // echo $payment_method. "<br>";
        // echo $payment_status. "<br>";
        //echo $balanceDiamond ."<br>";
    
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $updateOrderDetails = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($updateOrderDetails)
        {
            if(isset($_POST['next']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";

                if($balanceDiamond)
                {
                    array_push($tableName,"diamond");
                    array_push($tableValue,$balanceDiamond);
                    $stringType .=  "s";
                }
            
                array_push($tableValue,$userId);
                $stringType .=  "s";
                $updateUserDetails = updateDynamicData($conn,"users"," WHERE user_id = ? ",$tableName,$tableValue,$stringType);
                if($updateUserDetails)
                {
                    //echo "<script>alert('Successfully added shipping details!');</script>";
                }
                else
                {
                    echo "<script>alert('Fail to add user details!');window.location='../checkout.php'</script>"; 
                }
            }
        }
        else
        {
            echo "<script>alert('Fail to add shipping details!');window.location='../checkout.php'</script>"; 
        }
    }
    else
    {
        echo "<script>alert('ERROR 2');window.location='../checkout.php'</script>"; 
    }

         
}
else 
{
     header('Location: ../index.php');
}

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
// $userDetails = $userRows[0];

// $userOrder = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
// $orderDetails = $userOrder[0];

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}else
{}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="PPay" />
<title>PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay,e-commerce,iphone,phone,huawei">
<link rel="stylesheet" type="text/css" href="css/glider.css">
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="two-menu-space width100"></div>    

<div class="width100 same-padding min-height4">

    <?php
    if(isset($_POST['uid']))
    {
    $conn = connDB();
    $orders = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['uid']),"s");
    ?>
        <div class="dual-div">
            <p class="green-text top-text">Order No.</p>
            <p class="bottom-text"><?php echo $orders[0]->getId();?></p>
        </div>
        <div class="dual-div second-dual-div">
            <p class="green-text top-text">Order Confirm On</p>
            <p class="bottom-text"><?php echo $orders[0]->getDateCreated();?></p>
        </div> 
    <?php
    }
    ?>

    <?php
    if(isset($_POST['uid']))
    {
    $orderId = $_POST['uid'];
    $conn = connDB();
    //$orders = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
    $orders = getProductOrders($conn);
    $order = getOrders($conn);
    ?>
        <div class="width100 overflow">
            <p class="green-text top-text">Ordered Products</p>
            <div class="table-scroll-div">
                <table class="order-table">
                    <thead>	
                        <tr>
                            <th>No.</th>
                            <th>Product</th>
                            <th>Qty</th>
                            <th class="price-column">Sub-Total Diamonds</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $no=1;
                        for($cnt = 1;$cnt < count($orders) ;$cnt++){
                        if($orders[$cnt]->getOrderId() == $orderId)
                        {
                        ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $orders[$cnt]->getProductName();?></td>
                                <td><?php echo $orders[$cnt]->getQuantity();?></td>
                                <td class="price-column"><?php echo $orders[$cnt]->getTotalProductPrice();?></td>
                            </tr>
                        <?php
                        $no++;
                        }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <table class="price-table">
                <tbody>
                    <?php
                    if(isset($_POST['uid']))
                    {
                    $conn = connDB();
                    $orders = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['uid']),"s");
                    ?>
                        <tr>
                            <td class="last-td"><b>Total Diamonds</b></td>
                            <td class="last-td price-padding"><b><?php echo $orders[0]->getTotal();?></b></td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>                        
        </div>

        <div class="clear"></div>

        <div class="width100 overflow some-margin-bottom">
            <p class="green-text top-text">Delivery Address</p>
            <p class="bottom-text"><b>Mr/Miss <?php echo $orders[0]->getName();?></b> (<?php echo $orders[0]->getContactNo();?>)</p>
            <p class="bottom-text"><?php echo $orders[0]->getAddressLine1();?></p>     	
        </div>

        <div class="clear"></div>

        <div style="display: none" id="showLater">
            <div class="white-input-div payment-white-div">
                <input required type="time" id="payment_time" name="payment_time" value="<?php echo $time ?>">
            </div>
        </div>
    <?php
    }
    ?>

    <div class="clear"></div>  
    
    <div class="width100 text-center">                                                                         
        <!-- <input type="button" class="green-button checkout-btn clean" onclick="location.href='profile.php';" value="Completed" /> -->

        <!-- <form method="POST"> -->
        <form method="POST" action="profile.php">
            <!-- <button class="green-button checkout-btn clean" type="submit">Completed</button> -->
                <?php
                    $conn = connDB();
                    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
                    {
                        unset($_SESSION['shoppingCart']);
                    }
                    // $conn->close();
                ?>
                <button class="green-button checkout-btn clean" type="submit">Completed</button>
        </form>

    </div>
    
</div>

<?php include 'js.php'; ?>

<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.green-footer{
		display:none;}
</style>

</body>
</html>