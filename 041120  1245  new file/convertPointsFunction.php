<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/ConvertPointReport.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function convertPoints($conn,$uid,$userId,$userName,$currentPoint,$convertAmount,$currentDiamond,$diamond)
{
    if(insertDynamicData($conn,"convert_point",array("uid","user_id","user_name","current_points","convert_points","current_diamond","diamond"),
            array($uid,$userId,$userName,$currentPoint,$convertAmount,$currentDiamond,$diamond),"sssdddd") === null)
        {
            return false;
        }
    else
    {  }
    return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $userId = $_SESSION['user_id'];

    $uid = md5(uniqid());

    $userRows = getUser($conn, "WHERE user_id =?",array("user_id"),array($userId),"s");
    $userDetails = $userRows[0];
    // $userId = $userDetails->getUserId();
    $userName = $userDetails->getUsername();

    $currentPoint = rewrite($_POST["current_point"]);
    $currentDiamond = rewrite($_POST["current_diamond"]);

    $convertAmount = rewrite($_POST["convert_amount"]);

    $point = $currentPoint - $convertAmount;
    $convertionDiamond = ($convertAmount / 1000);
    $diamond = $currentDiamond + $convertionDiamond;

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $userId."<br>";
    // echo $userName."<br>";
    // echo $currentPoint."<br>";
    // echo $currentDiamond."<br>";
    // echo $convertAmount."<br>";
    // echo $point."<br>";
    // echo $diamond."<br>";

    if($convertAmount > $currentPoint)
    {
        $_SESSION['messageType'] = 2;
        header('Location: ../profile.php?type=5');
    }
    else
    {
        $user = getUser($conn," user_id = ? ",array("user_id"),array($userId),"i");    

        if(!$user)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
    
            if($point)
            {
                array_push($tableName,"point");
                array_push($tableValue,$point);
                $stringType .=  "d";
            }
            if(!$point)
            {
                array_push($tableName,"point");
                array_push($tableValue,$point);
                $stringType .=  "d";
            }
            if($diamond)
            {
                array_push($tableName,"diamond");
                array_push($tableValue,$diamond);
                $stringType .=  "s";
            }
            array_push($tableValue,$userId);
            $stringType .=  "i";
            $passwordUpdated = updateDynamicData($conn,"users"," WHERE user_id = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                if (convertPoints($conn,$uid,$userId,$userName,$currentPoint,$convertAmount,$currentDiamond,$diamond))
                {
                    $_SESSION['messageType'] = 2;
                    header('Location: ../profile.php?type=1');
                }
                else
                {
                    $_SESSION['messageType'] = 2;
                    header('Location: ../profile.php?type=2');
                }
            }
            else
            {
                $_SESSION['messageType'] = 2;
                header('Location: ../profile.php?type=3');
            }
        }
        else
        {
            $_SESSION['messageType'] = 2;
            header('Location: ../profile.php?type=4');
        }
    }
}
else 
{
    header('Location: ../index.php');
}
?>
