<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/AdditionalPoints.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addExtraPoints($conn,$uid,$userId,$name,$currentPoints,$extraPoints,$newPoints)
{
     if(insertDynamicData($conn,"additional_point",array("uid","user_id","user_name","current_points","extra_points","total_points"),
          array($uid,$userId,$name,$currentPoints,$extraPoints,$newPoints),"sssddd") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = md5(uniqid());

    $userId = rewrite($_POST["user_id"]);

    $name = rewrite($_POST["update_name"]);
    $username = rewrite($_POST["update_username"]);
    $phone = rewrite($_POST["update_phone"]);
    $email = rewrite($_POST["update_email"]);
    $currentPoints = rewrite($_POST["current_points"]);
    $extraPoints = rewrite($_POST["extra_points"]);

    $newPoints = $currentPoints + $extraPoints;

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";
    // echo $gender."<br>";
    // echo $dob."<br>";
    // echo $phone."<br>";
    // echo $email."<br>";

    $user = getUser($conn," user_id = ? ",array("user_id"),array($userId),"s");    

    if(!$user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($name)
        {
            array_push($tableName,"name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
        if($username)
        {
            array_push($tableName,"username");
            array_push($tableValue,$username);
            $stringType .=  "s";
        }
        if($phone)
        {
            array_push($tableName,"phone");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }
        if($newPoints)
        {
            array_push($tableName,"point");
            array_push($tableValue,$newPoints);
            $stringType .=  "s";
        }

        array_push($tableValue,$userId);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"users"," WHERE user_id = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            if($extraPoints != '')
            {
                if(addExtraPoints($conn,$uid,$userId,$name,$currentPoints,$extraPoints,$newPoints))
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../adminAllUsers.php?type=1');
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../adminAllUsers.php?type=4');
                }
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminAllUsers.php?type=1');
            }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminAllUsers.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../adminAllUsers.php?type=3');
    }

}
else 
{
    header('Location: ../index.php');
}
?>
