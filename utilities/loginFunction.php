<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['login'])){

        $username = rewrite($_POST['username']);
        $password = rewrite($_POST['password']);

        // //   FOR DEBUGGING
        // echo "<br>";
        // echo $username."<br>";
        // echo $password."<br>";

        $userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
        if($userRows)
        {
            $user = $userRows[0];

            // $tempPass = hash('sha256',$password);
            // $finalPassword = hash('sha256', $user->getSalt() . $tempPass);
    
                if($password == $user->getUserId()) 
                {
                    // if(isset($_POST['remember-me'])) 
                    // {
                    //     setcookie('name-mypetslib', $name, time() + (86400 * 30), "/");
                    //     setcookie('password-mypetslib', $password, time() + (86400 * 30), "/");
                    //     setcookie('remember-mypetslib', 1, time() + (86400 * 30), "/");
                    //     // echo 'remember me';
                    // }
                    // else 
                    // {
                    //     setcookie('name-mypetslib', '', time() + (86400 * 30), "/");
                    //     setcookie('password-mypetslib', '', time() + (86400 * 30), "/");
                    //     setcookie('remember-mypetslib', 0, time() + (86400 * 30), "/");
                    //     // echo 'null';
                    // }

                    $_SESSION['user_id'] = $user->getUserId();
                    $_SESSION['usertype'] = $user->getUserType();
                    
                    if($user->getUserType() == 0)
                    {
                        header('Location: ../adminDashboard.php');
                    }
                    elseif($user->getUserType() == 1)
                    {
                        header('Location: ../profile.php');
                        echo "user login success";
                    }
                    else
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../index.php?type=2');
                    }
                }
                else 
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../index.php?type=3');
                }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../index.php?type=4');
        }
    }

    $conn->close();
}
?>