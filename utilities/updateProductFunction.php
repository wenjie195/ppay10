<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $productUid = rewrite($_POST["product_uid"]);

    $category = rewrite($_POST["update_category"]);
    $brand = rewrite($_POST["update_brand"]);
    $productName = rewrite($_POST["update_product_name"]);

    // $stringOne = ($_POST['update_brand']);
    // $brandType = preg_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringOne);
    // $updatedBrand = str_replace(' ', '-', trim($brandType));
    $stringTwo = ($_POST['update_product_name']);
    $productName = preg_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringTwo);
    $updatedProductName = str_replace(' ', '-', trim($productName));
    $slash = "-";
    // $slug = $updatedBrand.$slash.$updatedProductName;
    $slug = $updatedProductName;

    $price = rewrite($_POST["update_price"]);
    $diamond = rewrite($_POST["update_diamond"]);
    $description = ($_POST['update_description']);

    $oriFileOne = rewrite($_POST["ori_fileone"]);
    $newFileOne = $_FILES['image_one']['name'];
    if($newFileOne == '')
    {
        $file = $oriFileOne;
    }
    else
    {
        $file = $oriFileOne;
        $file = $timestamp.$_FILES['image_one']['name'];
        $target_dir = "../uploads/";
        $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
        // Select file type
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Valid file extensions
        $extensions_arr = array("jpg","jpeg","png","gif","pdf");
        if( in_array($imageFileType,$extensions_arr) )
        {
            move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$file);
        }
    }

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    // $productNameRows = getProduct($conn," WHERE name = ? ",array("name"),array($_POST['update_product_name']),"s");
    // $existingProductName = $productNameRows[0];

    // if(!$existingProductName)
    // {
        $product = getProduct($conn," uid = ? ",array("uid"),array($productUid),"s");    
        if(!$product)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($category)
            {
                array_push($tableName,"category");
                array_push($tableValue,$category);
                $stringType .=  "s";
            }
            if($brand)
            {
                array_push($tableName,"brand");
                array_push($tableValue,$brand);
                $stringType .=  "s";
            }
            if($productName)
            {
                array_push($tableName,"name");
                array_push($tableValue,$productName);
                $stringType .=  "s";
            }
            
            if($slug)
            {
                array_push($tableName,"slug");
                array_push($tableValue,$slug);
                $stringType .=  "s";
            }

            if($price)
            {
                array_push($tableName,"price");
                array_push($tableValue,$price);
                $stringType .=  "s";
            }
            if($diamond)
            {
                array_push($tableName,"diamond");
                array_push($tableValue,$diamond);
                $stringType .=  "s";
            }
            if($description)
            {
                array_push($tableName,"description");
                array_push($tableValue,$description);
                $stringType .=  "s";
            }
            if($file)
            {
                array_push($tableName,"image_one");
                array_push($tableValue,$file);
                $stringType .=  "s";
            }

            array_push($tableValue,$productUid);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminViewAllProduct.php?type=1');
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminViewAllProduct.php?type=2');
            }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminViewAllProduct.php?type=3');
        }
    // }
    // else
    // {
    //     echo "Product name already used !!";
    // }

}
else 
{
    header('Location: ../index.php');
}
?>
