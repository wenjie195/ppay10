<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Category.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewCategory($conn,$category,$status)
{
     if(insertDynamicData($conn,"category",array("category","status"),
          array($category,$status),"ss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $category = rewrite($_POST['register_category']);
    $status = "Available";

    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $name."<br>";
    //  echo $status."<br>";

    $categoryNameRows = getCategory($conn," WHERE category = ? ",array("category"),array($_POST['register_category']),"s");
    $existingCategoryName = $categoryNameRows[0];

    if(!$existingCategoryName)
    {
        if(registerNewCategory($conn,$category,$status))
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminViewAllCategory.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminAddCategory.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../adminAddCategory.php?type=3');
    }

}
else 
{
     header('Location: ../index.php');
}

?>