<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Slider.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userID = $_SESSION['uid'];
$timestamp = time();

function addSlider($conn,$uid,$imgName,$link,$status)
{
     if(insertDynamicData($conn,"slider",array("uid","img_name","link","status"),
          array($uid,$imgName,$link,$status),"ssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $imgName = $uid.$timestamp.$_FILES['image_one']['name'];
     $target_dir = "../uploadsSlider/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imgName);
     }

     $link = rewrite($_POST['link']);
     $status = "Show";

    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $uid."<br>";
    //  echo $status."<br>";

     if(addSlider($conn,$uid,$imgName,$link,$status))
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../slider.php?type=1');
     }
     else 
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../slider.php?type=2');
     }
 
}
else 
{
     header('Location: ../index.php');
}

?>