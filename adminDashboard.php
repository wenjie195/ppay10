<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$userId = $_SESSION['user_id'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_id =?",array("user_id"),array($userId),"s");
$userData = $userDetails[0];

$userAmount = getUSer($conn," WHERE user_type = 1 ");
$productAmount = getProduct($conn," WHERE status = 'Available' ");

$orders = getOrders($conn, "WHERE payment_status = 'PENDING' AND shipping_status = 'PENDING' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Admin Dashboard | PPay" />
<title>Admin Dashboard | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance admin-dash">
	<h1 class="green-text h1-title"><?php echo _ADMIN_DASHBOARD ?></h1>
    <div class="green-border"></div>
    
    <div class="clear"></div>

        <div class="width100 border-separation">
        
            <div class="white-dropshadow-box four-div-box">
                <img src="img/pet-sellers.png" alt="<?php echo _ADMIN_ALL_USERS ?>" title="<?php echo _ADMIN_ALL_USERS ?>" class="four-div-img">
                <p class="four-div-p"><?php echo _ADMIN_TOTAL_USERS ?></p>
                <?php
                if($userAmount)
                {   
                    $totalUserAmount = count($userAmount);
                }
                else
                {   $totalUserAmount = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalUserAmount;?></b></p>                
                <!-- <a href="addPuppy.php"><p class="dashboard-p"><b>Add</b></p></a> -->
                <a href="adminAllUsers.php"><p class="dashboard-p dashboard-p2"><b><?php echo _ORDERS_VIEW ?></b></p></a>
                <!-- <a href="#"><p class="dashboard-p dashboard-p2"><b>View</b></p></a> -->
            </div>  

            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/product.png" alt="<?php echo _INDEX_PRODUCT ?>" title="<?php echo _INDEX_PRODUCT ?>" class="four-div-img">
                <p class="four-div-p"><?php echo _ADMIN_TOTAL_PRODUCT ?></p>
                <?php
                if($productAmount)
                {   
                    $totalProductAmount = count($productAmount);
                }
                else
                {   $totalProductAmount = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalProductAmount;?></b></p>
                <!-- <a href="addSeller.php"><p class="dashboard-p"><b>Add</b></p></a> -->
                <!-- <a href="seller.php"><p class="dashboard-p dashboard-p2"><b>View</b></p></a> -->
                <a href="adminViewAllProduct.php"><p class="dashboard-p dashboard-p2"><b><?php echo _ORDERS_VIEW ?></b></p></a>
            </div> 

            <div class="white-dropshadow-box four-div-box third-four-div-box right-four-div">
                <img src="img/order.png" alt="<?php echo _ORDERS_PENDING ?>" title="<?php echo _ORDERS_PENDING ?>" class="four-div-img">
                <p class="four-div-p"><?php echo _ORDERS_PENDING ?></p>
                <?php
                if($orders)
                {   
                    $totalPendingOrders = count($orders);
                }
                else
                {   $totalPendingOrders = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalPendingOrders;?></b></p>
                <!-- <a href="addSeller.php"><p class="dashboard-p"><b>Add</b></p></a> -->
                <!-- <a href="seller.php"><p class="dashboard-p dashboard-p2"><b>View</b></p></a> -->
                <!-- <a href="#"><p class="dashboard-p dashboard-p2"><b>Coming Soon</b></p></a> -->
                <a href="adminViewPendingOrders.php"><p class="dashboard-p dashboard-p2"><b><?php echo _ORDERS_VIEW ?></b></p></a>
            </div> 
    
        <div class="clear"></div>

        </div>
</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Password Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            // $messageType = "Fail to update password !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>