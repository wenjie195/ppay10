<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$userId = $_SESSION['user_id'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_id =?",array("user_id"),array($userId),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Password | PPay" />
<title>Edit Password | PPay</title>
<meta property="og:description" content="PPay" />
<meta name="description" content="PPay" />
<meta name="keywords" content="PPay">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

	<div class="width100 same-padding overflow min-height2 menu-distance text-center">
    	<p class="review-product-name">Change Password</p>
 		<!-- <form> -->
        <form action="utilities/editPasswordFunction.php" method="POST">
        <div class="center-content">
        	<p class="input-top-p ow-text-align-left">Current Password</p>
        	<div class="edit-password-input-div">
                <input class="input-name clean input-password edit-password-input" type="Password" placeholder="Current Password" id="password" name="password" required>   
                <p class="edit-p-password">
                    <img src="img/visible.png" class="hover1a edit-password-img" onclick="myFunctionC()" alt="View Password" title="View Password">
                    <img src="img/visible2.png" class="hover1b edit-password-img" alt="View Password" title="View Password">
                </p>               
            </div>

         
            <p class="input-top-p ow-text-align-left">New Password</p>
            <div class="edit-password-input-div">
            	<input class="input-name clean input-password edit-password-input"  type="Password" placeholder="New Password" id="register_password" name="register_password" required>
                <p class="edit-p-password">
                    <img src="img/visible.png" class="hover1a edit-password-img" onclick="myFunctionA()" alt="View Password" title="View Password">
                    <img src="img/visible2.png" class="hover1b edit-password-img" alt="View Password" title="View Password">
                </p>   
            </div>
            <p class="input-top-p ow-text-align-left">Retype New Password</p>
            <div class="edit-password-input-div">
            	<input class="input-name clean input-password edit-password-input"  type="Password" placeholder="Retype New Password" id="register_retype_password" name="register_retype_password" required>
                <p class="edit-p-password">
                    <img src="img/visible.png" class="hover1a edit-password-img" onclick="myFunctionB()" alt="View Password" title="View Password">
                    <img src="img/visible2.png" class="hover1b edit-password-img" alt="View Password" title="View Password">
                </p>
            </div> 
            <button class="green-button white-text clean width100">Submit</button>  
        </div>
        <div class="clear"></div>
        	
        </form>
    </div>
    
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Profile Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update password !! ";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "new password must be same with retype password !";
            // $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "password length must be more than 5 !";
            // $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "current password is wrong !";
            // $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>